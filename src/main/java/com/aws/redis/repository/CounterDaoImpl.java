package com.aws.redis.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;

@Repository
public class CounterDaoImpl {
    @Autowired
    private RedisTemplate redisTemplate;

    private static final String KEY = "COUNTER";

    @PostConstruct
    void postConstruct(){
        String redisKey = "counter" ;
        Integer counter = (Integer)redisTemplate.opsForHash().get(KEY, redisKey);
        if(ObjectUtils.isEmpty(counter)){
            System.out.println("The counter is missing in redis, please add this");
            // if we are storing plain primitives like String , Integer, Long we can use opsForValue
            // opsForHash - is used to store POJO as key-> [field1 -> value1, field2 -> value2 ...]
            // redisTemplate.opsForValue().set(redisKey, 10);
            redisTemplate.opsForHash().put(KEY, redisKey, 10);

        }else{
            System.out.println("################################################Before");
            System.out.println(counter.toString());
            counter -= 2;
            if(counter <= 0){
                redisTemplate.opsForHash().put(KEY, redisKey, 10);
            }else{
                redisTemplate.opsForHash().put(KEY, redisKey, counter);
            }
            //redisTemplate.opsForHash().put(KEY, redisKey, counter);
            counter = (Integer)redisTemplate.opsForHash().get(KEY, redisKey);
            System.out.println("################################################After");
            System.out.println(counter.toString());
        }
    }
}
