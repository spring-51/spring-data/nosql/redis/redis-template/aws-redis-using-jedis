package com.aws.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsRedisApplication.class, args);
	}

}
