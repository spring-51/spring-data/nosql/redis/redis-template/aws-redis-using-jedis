# Caching using REmote DIctionary Server (REDIS) via redis-template

### NOTE:
```
- In this example we have used jedis library to create bean of redis-template.
  Here jedis will be considered as redis client. 
    <dependency>
        <groupId>redis.clients</groupId>
        <artifactId>jedis</artifactId>
        <version>3.3.0</version>
    </dependency>
    
    - refer pom.xml 
```

## Redis Configuration

```
- In order to connect to redis server all we need is 
  - host
  - port
  remaining all other are **OPTIONAL**, the extra fields required 
  only if our redis server is set up that way. 
  for eg. if in order to connect to redis , it requires password
  then we need to add password to our configuration.
  
  - refer RedisConfig
```

## Caching POJO
```
- redis uses value as hashed to cache pojo.
  - redisTemplate.opsForHash()
  - refer UserDaoImpl 
```

#Caching Primitive value
```
- redis cache primitives as plain String.
  - redisTemplate.opsForValue()
- redis uses value as hashed to cache primitives as well.
  - redisTemplate.opsForHash()
  - refer CounterDaoImpl 
```

